const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")

router.post("/createCourse", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  courseController.createCourse({userId: userData.id}, req.body).then(result=>res.send(result))
})

router.get("/", (req,res)=>{
  courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))
})
/* 
  ecommerce websites

  create a route that will retrieve all of our products/courses

*/

/* 
  in getting all of the documents, in case we need multiple of them, place the route with the criteria to the find method first before getting the one with params
*/
router.get("/active", (req,res)=>{
  courseController.getActiveCourses().then(resultFromController=>res.send(resultFromController))
})

router.get("/:courseId", (req,res)=>{
  console.log(req.params.courseId)
  courseController.getCourses(req.params).then(resultFromController=>res.send(resultFromController))
});

router.put("/:courseId", auth.verify, (req,res)=>{
  courseController.updateCourse(req.params, req.body).then(result=>res.send(result))
})



/* 
use /archiveCourse and send a put request to archive a course by changing the active status
*/

router.put("/:courseId/archiveCourse", auth.verify, (req, res)=>{
  const userData = auth.decode(req.headers.authorization)
  courseController.archiveCourse(req.params, {userId: userData.id}, req.body).then(result=>res.send(result))
})

module.exports = router