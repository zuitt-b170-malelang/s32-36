const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

/* 
create a function with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController
reminder: we have a parameter needed in the checkEmail function and that is data from requestBody
*/
// checking email
router.post("/checkEmail", (req,res)=>{
  userController.checkEmail(req.body).then(result => res.send(result));
})
// user registration
router.post("/register", (req, res) =>{
  userController.registerUser(req.body).then(result=>res.send(result))
})
// user login
router.post("/login", (req, res)=>{
  userController.userLogin(req.body).then(result=>res.send(result))
})
// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details",auth.verify, (req,res)=>{
  // decode - decrypts the token inside the authorization (which is in the headers of the request)
  // req.headers.authorization contains the token that was created for the user
  const userData = auth.decode(req.headers.authorization)
  userController.getProfile({userId: userData.id}).then(result=>res.send(result))
})

router.post("/enroll",auth.verify, (req,res)=>{
  let data = {
    userId : auth.decode(req.headers.authorization).id,
    courseId: req.body.courseId
  }
  userController.enroll(data).then(result=>res.send(result))
})

/* 
  create a post request under "/enroll" endpoint
  check if the user is logged in
  creat a variable and store an object with userId and courseId as fields
  userId - decoded token of the user
  courseId- the courseId present in the rquest body

  under the variable as a parameter of the enrol function

  send a screenshot of your codes in our batch chat*/


module.exports = router