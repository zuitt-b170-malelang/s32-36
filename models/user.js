/* 
firstName - string
lastName - string
age - string
gender - string
email - string
password - string
mobileNo - string

isAdmin - boolean, default: false

enrollment array- will show the courses(id) where the students are enrolled

push s32-36 "add user schema"
*/

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "Firstname is required"]
  },
  lastName: {
    type: String,
    required: [true, "Lastname is required"]
  },
  age:{
    type: String,
    required: [true, "Age is required"]
  },
  gender:{
    type: String,
    required: [true, "Gender is required"]
  },
  email:{
    type:String,
    required: [true, "Email is required"]
  },
  password:{
    type:String,
    required: [true, "Password is required"]
  },
  mobileNo:{
    type:String,
    required: [true, "Mobile Number is required"]
  },
  isAdmin:{
    type:Boolean,
    default: false
  },
  enrollments:[
    {
      courseId:{
        type: String,
        required: [true, "Course ID is required"]
      },
      enrolledOn:{
        type:Date,
        default: new Date()
      },
      status:{
        type:String,
        default: "Enrolled"
      }
      
    }
  ]

})

module.exports = mongoose.model("User", userSchema)