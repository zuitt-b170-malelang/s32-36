const jwt = require("jsonwebtoken")
const secret = "IDoNotKnow" //Form of message/string in which this will serve as our secret code

// JSON WebToken - is a way of securely passing of information from a part of the server to the front end.
// Info is kept secure through the use of secret variable
// the secret variable is only known by the system/browser which can be decoded by the server

// token creation

module.exports.createAccessToken = (user) => {
  // data will be received from the registration form, when the user login, a token will be created with the user's information(this info is styll encrypted inside the token)
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  // generates the token using the form data and the secret code without additional options
  return jwt.sign(data, secret, {})
}


// token verification - 

module.exports.verify = (req, res, next) => { //next parameter - tells the server to proceed if the verification is okay/successful
  let token = req.headers.authorization //authorization can be found in the headers of the request and tells whether the client/user has the authority to send the request

  if(typeof token !== "undefined"){//token is present
    console.log(token)
    token = token.slice(7, token.length)

      // jwt verify - verifies the token using the secret, and fails if the token's secret does not match the secret variable meaning there is an attempt to hack, or atleast to tamper/change the data from the user-end
    return jwt.verify(token, secret, (err, data)=>{
      if(err){
        return res.send({auth:"failed"})
      }else{

        next()
      }
    })
  }
}

// token decoding
module.exports.decode = (token) =>{
  if(typeof token !== "undefined"){
    // token is present
    token = token.slice(7, token.length)
    return jwt.verify(token, secret, (err,data)=>{
      if (err){
        console.log(err)
        return null
      }else{
        return jwt.decode(token, {complete: true}).payload
        // jwt.decode - decides the data to be decoded, which is the token
        // payload - is the one that we need to veerify the user information. This is part of the token when code the createAccessToken function (the one with _id, email and isAdmin)
      }
    })
  } else {
    return null //  there is no token
  }
}