const User = require("../models/user.js")
const Course = require("../models/course.js")

module.exports.createCourse = (data, reqBody) => {
  return User.findById(data.userId).then(result=>{
    if(result === null){
      return false
    }else if(result.isAdmin === false){
        return "You are not an admin"
      }else{
        let newCourse = new Course({
          name: reqBody.name,
          description: reqBody.description,
          price:reqBody.price,
          isActive:reqBody.isActive,
          createdOn:reqBody.createdOn,
          enrollees:reqBody.enrollees,
          enrolledOn:reqBody.enrolledOn
        })
        return newCourse.save().then((saved, error)=>{
          if(error){
            console.log(error)
            return false
          }else{
            return "Course Created Successfully"
          }
        })
      }
  })
}

module.exports.getAllCourses = () =>{
  return Course.find({}).then((result,error)=>{
    if(error){
      return false
    }else{
      return result
    }
  })
}

/* 
using the course id (params) in the url, retrieve a course using a get request
*/

module.exports.getCourses = (reqParams) =>{
  return Course.findById(reqParams.courseId).then((result,error)=>{
    if(error){
      return false
    }else{
      return result
    }
  })
};

// Active courses
module.exports.getActiveCourses = () =>{
  return Course.find({isActive: true}).then((result, error)=>{
    if(error){
      return false
    }else{
      return result
    }
  })
}

// update a course
module.exports.updateCourse = (reqParams, reqBody) =>{
  let updatedCourse = {
    name : reqBody.name,
    description : reqBody.description,
    price:reqBody.price
  }
  // findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
    //the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error)=>{
    if(error){
      return false
    }else{
      return true
    }
  })
}

// archive a course
/* 
create an updateCourse object with the content from the reqBody
  reqBody should have the isActive status of the course to be set to false

  find the course by its id and update with the updateCourse object using findByIdAndUpdate
    handle the errors that may arise
      error/s = false
      no error = true
*/

module.exports.archiveCourse = (reqParams, data, reqBody) =>{
  console.log(reqParams)
  let updateCourse = {
    isActive: reqBody.isActive
  }
  return User.findById(data.userId).then((result, error)=>{
    if(error){
      return false
    }else{
      if(result === null){
        return false
      }else if(result.isAdmin === false){
        return "You are not an admin"
      }else{
        return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((result, error)=>{
          if(error){
            return false
          }
          else{
            return true
          }
      })

      }
    }
  })
}