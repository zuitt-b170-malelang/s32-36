// setup dependencies

const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")
const user = require("../models/user.js")
const course = require("../models/course.js")

// check if the email exists
/* 
1. check for the email in the database
2/ send the result as a response with error handling
*/

/* 
it is conventional for the devs to use boolean in sending return responses esp with the backend applciation
*/
module.exports.checkEmail = (requestBody) => {
  return User.find({email: requestBody.email}).then((result, error)=> {
    if(error){
      console.log(error)
      return false
    }else{
      if(result.length>0){
        // return result
        return true
      }else{
        // rturn res.send("email does not exist")
        return false
      }
    }
  })
}

/* 
USER REGISTRATION
1. create a new User object with the information from the requestBody
2.make sure that the password is encrypted
3. save the new user
*/

module.exports.registerUser = (reqBody) =>{
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    age:reqBody.age,
    gender:reqBody.gender,
    email:reqBody.email,
    // hashSync - function of bcrypt that encrypts the password
        // 10 - number of rounds/times it runs the algorithm to the reqBody.password
          // max is 72
    password:bcrypt.hashSync(reqBody.password, 10),
    mobileNo:reqBody.mobileNo
  })
  return newUser.save().then((saved, error)=>{
    if(error){
      console.log(error)
      return false
    }else{
      return true
    }
  })
}

// USER LOGIN
/* 
1. find if the email is existing in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
  return User.findOne({email: reqBody.email}).then(result=>{
    if(result === null){
      return false
    }else{
      // compareSync - used to compare a non-encrypted password to an encrypted password and returns a Boolean response depending on the result
      /* 
      true - a token should be created since the user is existing and the password is correct
      false - the passwords do not match, thus a token should not be created
      */
      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

      if(isPasswordCorrect){
        return {access: auth.createAccessToken(result.toObject())}
        // auth - imported auth.js
        // createAccessToken - function inside the auth.js to create access token
        // .toObject - transforms the User into an Object that can be used by our createAccessToken
      }
    }
  })
}

/* 
  create getProfile function inside userController
  1. find the id of the user using "data" as parameters
  2. if it does not exist, return false
  3. otherwise, return the details (stretch - make the password invisible)
*/
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then(result=>{
    if(result === null){
      return false
    }else{
      console.log(result)
      result.password = ""
      return result
    }
})
}


// ACTIVITY
/* 
1. find the user/document in the database
2. add the courseId to the user's enrollment array
3. save the document in the database
*/
module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then(user=>{
    // adding the courseId to the user's enrollment array
    user.enrollments.push({courseId: data.courseId})

    // saving in the database
    return user.save().then((user, error)=>{
      if(error){
        return false
      }else{
        return true
      }
    })
  })
  await Course.findById(data.courseId).then(course=>{
    course.enrollees.push({userId: data.userId})
    return course.save().then((course, error)=>{
      if(error){
        return false
      }else{
        return true
      }
    })
  })
  if (isUserUpdated){
    return true
  }else{
    return false
  }
}

/* 

  use another await keyword to update the enrollees array in the course collection


  find th courseId of the enrollee in the enrollees array of the course
  update the document in the database

  if both pusing are successful, return true
  if both pushing are not successful, return false
*/