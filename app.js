const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")


// access to routes
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

// server
const app = express()
const port = 4000

app.use(cors())//allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded({extended: true}))



mongoose.connect("mongodb+srv://evmalelang1:badminton05@wdc028-course-booking.jteye.mongodb.net/b170-course-booking?retryWrites=true&w=majority", {
useNewUrlParser: true,
useUnifiedTopology: true
})
let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", ()=> console.log("We're connected to the database"))


// defines the routes where the CRUD operations will be executed on the user ("/api/users") and course ("/api/courses")
app.use("/api/users", userRoutes)
app.use("/api/courses", courseRoutes)
app.listen(port,()=> console.log(`API now online at port ${port}`))